import React from 'react'
import { Link } from "react-router-dom";
import { Typography, Card, CardContent, CardMedia } from "@mui/material";
import CheckCircleIcon from "@mui/icons-material/CheckCircle";

import { demoThumbnailUrl, demoVideoUrl, demoVideoTitle, demoChannelUrl, demoChannelTitle } from "../utils/constant";
const VideoCard = ({ video: { id: { videoId }, snippet } }) => {

  return (
    <Card sx={{width:{md:'300px',xs:'80%'}, boxShadow:"0", borderRadius:"none"} }>
      <Link to={videoId ? `/video/${videoId}` :demoVideoUrl} >
        <CardMedia
        alt={snippet?.title}
        sx={{width:358,height:180}}
        image={snippet?.thumbnails?.high?.url} />
      </Link>
      <CardContent sx={{ backgroundColor:'#1e1e1e', height:'106px'}}>
      <Link to={videoId ? `/video/${videoId}` :demoVideoUrl} >
      <Typography variant="subtitle1" fontWeight="bold" color ="#FFF">{snippet?.title.slice(0,60)|| demoVideoTitle.slice(0,60)}</Typography>
      </Link>
      <Link to={snippet?.channelId ? `/video/${snippet?.ChannelId}` :demoChannelUrl} >
      <Typography variant="subtitle2" fontWeight="bold" color ="gray">{snippet?.channelTitle|| demoChannelTitle}
      <CheckCircleIcon sx={{fontSize:12, color:"gray", ml: "5px"}} />
      </Typography>
      
      </Link>

      
      
      </CardContent>

    </Card>
  )
}

export default VideoCard